let meal_data = {};
// Your code here...
let diary_rows = (document.getElementById("diary-table")).getElementsByTagName("tr")
let meal_type = "nada";
for (let i = 0; i < diary_rows.length; i++) {
	if (diary_rows[i].className == "meal_header") {
		//console.log("header");
		let columns = diary_rows[i].getElementsByTagName("td");
		for (let col_i = 0; col_i < columns.length; col_i++) {
			if (typeof(columns[col_i]) == "undefined") {
				continue;
			}
			if (columns[col_i].hasAttribute("class")) {
				if (columns[col_i].className == "first alt") {
					meal_type = columns[col_i].innerHTML;
					if (!(meal_type in meal_data)) {
						meal_data[meal_type] = [];
					}
				}
			}
		}
	} else {
		if ( diary_rows[i].classList[0] === undefined ){
			if(meal_type !== "nada"){
				//console.log("got a food!", meal_type);
				let entry = {
					"food":null,
					"calories":null,
					"carbs_val":null,
					"fat_val":null,
					"protein_val":null,
					"carbs_pct":null,
					"fat_pct":null,
					"protein_pct":null,
					"sugar":null,
					"fiber":null
				}
				let columns = diary_rows[i].getElementsByTagName("td");
				entry.food = columns[0].getElementsByTagName("a")[0].textContent;
				entry.calories = columns[1].textContent;
				entry.carbs_val = columns[2].getElementsByTagName("span")[0].textContent;
				entry.carbs_pct = columns[2].getElementsByTagName("span")[1].textContent;
				entry.fat_val = columns[3].getElementsByTagName("span")[0].textContent;
				entry.fat_pct = columns[3].getElementsByTagName("span")[1].textContent;
				entry.protein_val = columns[4].getElementsByTagName("span")[0].textContent;
				entry.protein_pct = columns[4].getElementsByTagName("span")[1].textContent;
				entry.sugar = columns[5].textContent;
				entry.fiber = columns[6].textContent;
				meal_data[meal_type].push(entry);
			}
		} else {
			meal_type = "nada";
			//console.log("reset", meal_type);
		}
	}
}
//console.log(meal_data);
//console.log(diary_rows);
return JSON.stringify(meal_data);
