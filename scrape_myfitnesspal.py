"""
This is a simple script for scraping data from myfitnesspal.
"""
import os
from datetime import datetime
from datetime import timedelta
import time
import json
import glob
import re
from selenium import webdriver
import numpy as np
import pandas as pd

_LOGIN_URL = "https://www.myfitnesspal.com/account/login"
_MYFITNESSPAL_USERNAME = os.getenv("MYFITNESSPAL_USERNAME")
_MYFITNESSPAL_PASSWORD = os.getenv("MYFITNESSPAL_PASSWORD")
_SCREENSHOT_DIRECTORY = "./screenshots"
_DIARY_PAGE_DIRECTORY = "./diary_html"
_PROCESSED_DIARY_PAGE_DIRECTORY = "./processed"
_EMPTY_DATES_FILENAME = "./empty_dates.json"


with open ("./scrape_diary.js", "r") as infile:
    lines = infile.readlines()
    _DIARY_SCRAPE_JS = ''.join(lines)

if not os.path.exists(_SCREENSHOT_DIRECTORY):
    os.mkdir(_SCREENSHOT_DIRECTORY)

if not os.path.exists(_DIARY_PAGE_DIRECTORY):
    os.mkdir(_DIARY_PAGE_DIRECTORY)

if not os.path.exists(_PROCESSED_DIARY_PAGE_DIRECTORY):
    os.mkdir(_PROCESSED_DIARY_PAGE_DIRECTORY)

if os.path.exists(_EMPTY_DATES_FILENAME):
    with open(_EMPTY_DATES_FILENAME, 'r') as empty_dates_file:
      _EMPTY_DATES = json.load(empty_dates_file)
else:
      _EMPTY_DATES = []

def static_vars(**kwargs):
    """ Decorate a function to add a static variable.

    Usage:
        @static_vars(counter=0) # (initialization)
        def foo():
            foo.counter += 1
            # do stuff
            # return stuff
    """
    def decorate(func):
        """modify the internals of func to add a static var"""
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate


def get_webdriver():
    """setup and return a selenium webdriver"""
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')

    # add a user-agent (don't broadcast headless)
    chrome_options.add_argument('--user-agent="Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 640 XL LTE) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Edge/12.10166"')

    driver = webdriver.Chrome(
        chrome_options=chrome_options,
        service_args=[
            '--verbose',
            '--log-path=/tmp/chromedriver.log'
            ]
    )
    driver.set_window_size(1920, 1080)
    return driver


@static_vars(counter=0)
def save_screenshot(driver):
    """oneliner wrapper to save a screenshot to the screenshot directory"""
    out_file_name = os.path.join(
        _SCREENSHOT_DIRECTORY,
        "{:%Y-%M-%d_%H:%M:%S}_{}.png".format(
            datetime.now(), 
            save_screenshot.counter
        )
    )
    save_screenshot.counter += 1
    driver.get_screenshot_as_file(out_file_name)


def mfp_login(driver):
    """Takes an initiated chromedriver and logs it into myfitnesspal."""
    print("initial page load")
    driver.get(_LOGIN_URL)

    print("logging in")
    driver.execute_script(
        "document.getElementById('username').value = '{}';".format(
            _MYFITNESSPAL_USERNAME
        )
    )
    driver.execute_script(
        "document.getElementById('password').value = '{}';".format(
            _MYFITNESSPAL_PASSWORD
        )
    )

    # Click on anything that might be "submit"
    driver.execute_script("document.querySelector('.submit').children[0].click();")
    print("logged in")

    return driver


def download_diary_page(driver, date_string):
    """Given a logged in driver, download the diary page for date.

    :param driver: A selnium webdriver that has been logged into a myfitnesspal
        session.
    :param date_string: A date-string YYYY-MM-DD

    """
    global _EMPTY_DATES
    print("Downloading diary page for date {date:}".format(date=date_string))
    date_url = "https://www.myfitnesspal.com/food/diary?date={date}"
    scrape_url = date_url.format(date=date_string)

    print(scrape_url)
    out_path = os.path.join(
        _DIARY_PAGE_DIRECTORY,
        "diary_{date:}.html".format(date=date_string)
    )

    # check if empty and seen before, or if we've scraped before
    if os.path.exists(out_path) or date_string in set(_EMPTY_DATES):
        print("exists, skipping")
        return driver

    driver.get(scrape_url)

    # Check if there are calories for the day
    rows = int(driver.execute_script(
        'return document.getElementsByTagName("tbody")[0].getElementsByTagName("tr").length'
    ))
    print("ROW CHECK", rows)
    if rows == 14: # diary is empty
        print("diary is empty, skipping")
        _EMPTY_DATES.append(date_string)
        with open(_EMPTY_DATES_FILENAME, 'w') as out_dates:
          json.dump(_EMPTY_DATES, out_dates)
        return driver

    food = str(driver.execute_script(_DIARY_SCRAPE_JS))
    food_data = json.loads(food)

    clean_data = []
    for meal in food_data:
        for i in range(len(food_data[meal])):
            clean_data.append({
                'meal'            : meal.strip().lower(),
                'food'            : food_data[meal][i]['food'].strip().lower(),
                'carbs_g'         : float(food_data[meal][i]['carbs_val'].replace(",","")),
                'calories'        : float(food_data[meal][i]['calories'].replace(",","")),
                'fat_g'           : float(food_data[meal][i]['fat_val'].replace(",","")),
                'protein_g'       : float(food_data[meal][i]['protein_val'].replace(",","")),
                # 'carbs_frac_dv'   : float(food_data[meal][i]['carbs_pct']) / 100,
                # 'fat_frac_dv'     : float(food_data[meal][i]['fat_pct']) / 100.,
                # 'protein_frac_dv' : float(food_data[meal][i]['protein_pct']) / 100.,
                'sugar_g'         : float(food_data[meal][i]['sugar'].replace(",","")),
                'fiber_g'         : float(food_data[meal][i]['fiber'].replace(",",""))
            })
    out_df = pd.DataFrame(clean_data)

    source = str(driver.page_source)
    with open(out_path, "w") as out_file:
        out_file.write(source)

    out_df_path = os.path.join(_PROCESSED_DIARY_PAGE_DIRECTORY, "dataframe_{:}.pickle".format(date_string))
    print("Saving to", out_df_path)
    out_df.to_pickle(out_df_path)
    print("date is finished")
    return driver


def test_run():
    """download something"""
    driver = get_webdriver()
    driver = mfp_login(driver)
    driver = download_diary_page(driver, "2018-09-12")
    driver.close()


def date_range(datetime_start, datetime_end):
    """ Returns a list of strings for dates"""
    return_arr = []
    while datetime_start <= datetime_end:
        return_arr.append("{date:%Y-%m-%d}".format(date=datetime_start))
        datetime_start += timedelta(days=1)
    return return_arr


def find_latest_date(directory=_PROCESSED_DIARY_PAGE_DIRECTORY):
    """finds the latest dated filename from directory.
    """
    regex_string = r".*dataframe_(\d\d\d\d-\d\d-\d\d).pickle"
    regex = re.compile(regex_string)
    files = glob.glob(os.path.join(directory, "*.pickle"))
    dates = []
    return min([datetime.strptime(regex.match(fname).groups()[0], '%Y-%m-%d') for fname in files])

def main():
    """Run the main thread, download five years of data"""
    last_date = find_latest_date(_PROCESSED_DIARY_PAGE_DIRECTORY)
    # print("starting from", last_date, "working backwards")
    # dates = date_range(datetime(2013,1,1), last_date)[::-1]
    dates = date_range(datetime(2010,11,21), datetime.now())[::-1]
    driver = get_webdriver()
    driver = mfp_login(driver)
    for date in dates:
        download_diary_page(driver, date)
        time.sleep(np.random.rand()*1.5)
    print("Finished!")

if __name__ == "__main__":
    main()
