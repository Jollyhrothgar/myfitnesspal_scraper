# Introduction

This library is used to scrape your MyFitnessPal data and store it in an sqlite
database.

# Setup

You must have your myfitnesspal username and password stored in an environment
variable, i.e.:

<code>MYFITNESSPAL\_USERNAME</code>
<code>MYFITNESSPAL\_PASSWORD</code>
